﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private NavMeshAgent navigation;
    public ZombieSpawner spawner { get; set; }

    void Start()
    {
        navigation = GetComponent<NavMeshAgent>();
        StartCoroutine(Track());
    }

    IEnumerator Track()
    {
        yield return new WaitForSeconds(1);
        var path = new NavMeshPath();
        if (navigation.CalculatePath(spawner.TargetPosition, path))
        {
            navigation.SetPath(path);
        }
        else
        {
            navigation.SetDestination(spawner.TargetPosition);
        }
        StartCoroutine(Track());
    }
}
