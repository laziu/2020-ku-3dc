﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Player : MonoBehaviour
{
    public UIManager ui;
    public PlayerSwapOnGameClear swapManager;

    private Animator animator;
    private ThirdPersonUserControl controller;
    private Stopwatch watch;

    private int life = 3;

    void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<ThirdPersonUserControl>();
        watch = new Stopwatch();
        watch.Start();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "DeadZone":
                GameOver();
                break;
            case "ClearZone":
                ui.GameClear();
                swapManager.GameClear();
                break;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Enemy":
                if (watch.ElapsedMilliseconds < 1000) break;
                watch.Restart();
                life--;
                ui.SetLife(life);
                if (life <= 0)
                {
                    GameOver();
                }
                else
                {
                    animator.Play("Damaged");
                }
                break;
        }
    }

    void GameOver()
    {
        animator.Play("Dead");
        controller.enabled = false;
        ui.GameOver();
    }
}
