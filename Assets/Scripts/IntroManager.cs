﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    public GameObject uiRoot;
    public UnityStandardAssets.Cameras.FreeLookCam playerCamera;
    public UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl playerController;
    public UIManager ui;
    public ZombieSpawner zspawner;

    private Animator animator;
    private bool running;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (running && !animator.GetCurrentAnimatorStateInfo(0).IsName("Intro"))
        {
            playerCamera.m_LookAngle = 0;
            playerCamera.m_TiltAngle = 0;
            playerController.enabled = true;
            ui.GameStart();
            Destroy(gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Space) && !running)
        {
            running = true;
            animator.Play("Intro");
            playerController.enabled = false;
            zspawner.GameStart();
            StartCoroutine(DestroyUI());
        }
    }

    IEnumerator DestroyUI()
    {
        yield return new WaitForSeconds(5);
        Destroy(uiRoot);
    } 
}
