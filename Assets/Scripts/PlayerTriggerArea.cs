﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTriggerArea : MonoBehaviour
{
    [Serializable]
    public class OnTriggerEvent : UnityEvent<string> { }

    public OnTriggerEvent onTriggerEvent;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onTriggerEvent.Invoke("GameOver");
        }
    }
}
