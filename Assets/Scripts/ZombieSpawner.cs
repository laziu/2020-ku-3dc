﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject[] prePlacedObjects;
    public GameObject[] zombiePrefabs;

    public GameObject player;
    public Vector3 TargetPosition { get; set; }

    private int zombieIndex = 0;

    public void GameStart()
    {
        foreach (var po in prePlacedObjects)
        {
            po.SetActive(true);
            po.GetComponent<Enemy>().spawner = this;
        }
        StartCoroutine(UpdateTargetPosition());
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(3);

        Vector3 pos;
        NavMeshHit hit;
        do
        {
            pos = new Vector3(Random.Range(-650, 650), 600, Random.Range(-650, 650));
        } while (!NavMesh.SamplePosition(pos, out hit, 650.0f, NavMesh.AllAreas));

        zombieIndex = (zombieIndex % (1 << (zombiePrefabs.Length - 1))) + 1;
        var enemy = Instantiate(zombiePrefabs[lsbPosition(zombieIndex) % zombiePrefabs.Length], hit.position, Quaternion.identity);
        enemy.GetComponent<Enemy>().spawner = this;
        StartCoroutine(Spawn());
    }

    IEnumerator UpdateTargetPosition()
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(player.transform.position, out hit, 650.0f, NavMesh.AllAreas);
        TargetPosition = hit.position;

        yield return new WaitForSeconds(0.25f);
        StartCoroutine(UpdateTargetPosition());
    }

    int lsbPosition(int bits)
    {
        int i = 0;
        for (; i < 31 && (bits & 1) == 0; bits >>= 1)
            ++i;
        return i;
    }
}
