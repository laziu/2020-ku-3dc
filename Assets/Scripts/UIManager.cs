﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI lifeText;
    public GameObject clearedText;
    public GameObject gameOverScreen;

    private bool started = false;
    private Stopwatch watch;

    public void SetLife(int life)
    {
        lifeText.text = new string('♥', life);
    }

    void Start()
    {
        watch = new Stopwatch();
    }

    void Update()
    {
        if (started)
        {
            timerText.text = ms2str(watch.ElapsedMilliseconds);
        }
    }

    public void GameStart()
    {
        started = true;
        watch.Start();
        lifeText.enabled = true;
    }

    public void GameClear()
    {
        clearedText.SetActive(true);
        watch.Stop();
    }

    public void GameOver()
    {
        gameOverScreen.SetActive(true);
    }

    private string ms2str(long ms)
    {
        var min = (ms / 1000) / 60;
        var sec = (ms / 1000) % 60;
        var pts = ms % 1000;
        return string.Format($"{min:00}:{sec:00}.{pts:000}");
    }
}
