﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Vehicles.Aeroplane;

public class PlayerSwapOnGameClear : MonoBehaviour
{
    public ThirdPersonUserControl playerController;
    public AeroplaneUserControl4Axis airplaneController;
    public ZombieSpawner zombieSpawner;
    public GameObject[] toDelete;

    public void GameClear()
    {
        foreach (var o in toDelete)
        {
            Destroy(o);
        }

        airplaneController.enabled = true;
        airplaneController.tag = "Player";
        airplaneController.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        playerController.enabled = false;
        playerController.tag = "Untagged";
        zombieSpawner.player = airplaneController.gameObject;
        Destroy(playerController.gameObject);
    }
}
